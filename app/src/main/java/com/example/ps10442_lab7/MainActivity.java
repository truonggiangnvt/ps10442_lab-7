package com.example.ps10442_lab7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btn1, btn2, btn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        map();
        setbtn1();
        setBtn2();
        setBtn3();
    }
    private void map(){
         btn1 = findViewById(R.id.btnExercise1);
         btn2 = findViewById(R.id.btnExercise2);
         btn3 = findViewById(R.id.btnExercise3);
    }
    private void setbtn1(){
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Exercise1.class));
            }
        });
    }
    private void setBtn2(){
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Exercise2.class));
            }
        });
    }
    private void setBtn3(){
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Exercise3.class));
            }
        });
    }
}
