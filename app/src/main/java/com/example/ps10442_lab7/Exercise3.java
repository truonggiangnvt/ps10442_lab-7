package com.example.ps10442_lab7;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Exercise3 extends AppCompatActivity {
    ImageView imgHour, imgMinute, imgSecond;
    Button btnRun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise3);
        map();
        setBtnRun();
    }
    private void map(){
        imgHour = findViewById(R.id.imageHour);
        imgMinute = findViewById(R.id.imageMinute);
        imgSecond = findViewById(R.id.imageSecond);
        btnRun = findViewById(R.id.btnRun);
    }
    private void setBtnRun(){
        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    ObjectAnimator animator2 = ObjectAnimator.ofFloat(imgSecond, "rotation", 360);
                    animator2.setDuration(60000);
                    animator2.setRepeatCount(720);
                    animator2.start();
                    ObjectAnimator animator = ObjectAnimator.ofFloat(imgHour, "rotation", 360);
                    animator.setDuration(43200000);
                    animator.start();
                    ObjectAnimator animator1 = ObjectAnimator.ofFloat(imgMinute, "rotation", 360);
                    animator1.setDuration(3600000);
                    animator1.setRepeatCount(12);
                    animator1.start();


            }
        });
    }
}
