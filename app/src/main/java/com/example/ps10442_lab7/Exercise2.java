package com.example.ps10442_lab7;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Exercise2 extends AppCompatActivity {
    ImageView imageView;
    Button btnOnePiece, btnLuffy, btnZoZo, btnSanji;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise2);
        map();
        setBtnOnePiece();
        setBtnLuffy();
        setBtnZoZo();
        setBtnSanji();
    }
    private void map(){
        imageView = findViewById(R.id.imageOnePice);
        btnOnePiece = findViewById(R.id.btnOnePiece);
        btnLuffy = findViewById(R.id.btnLuffy);
        btnZoZo = findViewById(R.id.btnZozo);
        btnSanji = findViewById(R.id.btnSanji);
    }
    private void setBtnOnePiece(){
        btnOnePiece.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("All");
            }
        });
    }
    private void setBtnLuffy(){
        btnLuffy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("Luffy");
            }
        });
    }
    private void setBtnZoZo(){
        btnZoZo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("ZoZo");
            }
        });
    }
    private void setBtnSanji(){
        btnSanji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("Sanji");
            }
        });
    }

    private void showImage(String image){
        ObjectAnimator anim = ObjectAnimator.ofFloat(imageView,"translationX",0f,500f);
        anim.setDuration(2000);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(imageView,"alpha",1f,0f);
        anim1.setDuration(2000);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(imageView,"translationX",-500f,0f);
        anim2.setDuration(2000);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(imageView,"alpha",0f,1f);
        anim3.setDuration(2000);
        AnimatorSet ans = new AnimatorSet();
        ans.play(anim2).with(anim3).after(anim).after(anim1);
        ans.start();
        final String imgName  = image;
        anim1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (imgName == "All"){
                    imageView.setImageResource(R.drawable.onepiceall);
                }
                if (imgName == "Luffy"){
                    imageView.setImageResource(R.drawable.luffy);
                }
                if (imgName == "ZoZo"){
                    imageView.setImageResource(R.drawable.zozo);
                }
                if (imgName == "Sanji"){
                    imageView.setImageResource(R.drawable.sanji);
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }
}
