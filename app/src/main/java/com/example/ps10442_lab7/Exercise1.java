package com.example.ps10442_lab7;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

public class Exercise1 extends AppCompatActivity {
    ImageView imageView;
    Button btnRotation, btnMoving, btnZoom;
    int dest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise1);
        map();
        setBtnRotation();
        setBtnZoom();
        setBtnMoving();
    }
    private void map(){
        imageView = findViewById(R.id.imageTheRock);
        btnRotation = findViewById(R.id.btnRotation);
        btnMoving = findViewById(R.id.btnMoving);
        btnZoom = findViewById(R.id.btnZoom);
    }
    private void setBtnRotation(){
        btnRotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dest = 360;
                if (imageView.getRotation() == 360){
                    System.out.println(imageView.getAlpha());
                    dest = 0;
                }
                ObjectAnimator animator = ObjectAnimator.ofFloat(imageView,"rotation",dest);
                animator.start();
            }
        });
    }
    private void setBtnMoving(){
        btnMoving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = new TranslateAnimation(Animation.ABSOLUTE,250,Animation.ABSOLUTE,Animation.ABSOLUTE);
                animation.setDuration(1000);
                animation.setFillAfter(true);
                imageView.startAnimation(animation);
            }
        });
    }
    private void setBtnZoom(){
        btnZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(Exercise1.this,R.anim.zoom);
                imageView.startAnimation(animation);
            }
        });
    }
}
